import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { login, logout, isLoggedIn } from '../utils/AuthService';


function Nav() {
    return (
        <Router>
            <nav className="navbar navbar-default">
            <div className="navbar-header">
            <Link className="navbar-brand" to="/">MiniVideos</Link>
            </div>
            <ul className="nav navbar-nav">
            <li>
                <Link to="/">Todos Los Videos</Link>
            </li>
            <li>
                {
                ( isLoggedIn() ) ? <Link to="/upload">Subir Videos</Link> :  ''
                }
            </li>
            </ul>
            <ul className="nav navbar-nav navbar-right">
            <li>
            {
                (isLoggedIn()) ? ( <button className="btn btn-danger log" onClick={() => logout()}>Desloguearse </button> ) : ( <button className="btn btn-info log" onClick={() => login()}>Iniciar Sesion</button> )
            }
            </li>
            </ul>
        </nav>
      </Router>
    );
}




export default Nav;
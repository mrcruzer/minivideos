import React from 'react';
import { Link } from 'react-router-dom';


function Upload() {

  const uploadWidget = () => {
    window.cloudinary.openUploadWidget(
      { cloud_name: 'dquajzsvc',
        upload_preset: 'qqof0klw',
        tags: ['minivideos'],
        sources: ['local', 'url', 'google_photos', 'facebook', 'image_search']
      },
      function(error, result) {
          console.log("This is the result of the last upload", result);
      });
  }


    return (
        <div>
          <h3 className="text-center">Subir Su Video</h3>
          <hr/>
  
          <div className="col-sm-12">
            <div className="jumbotron text-center">
              <button onClick={uploadWidget} className="btn btn-lg btn-info">Subir Video</button>
            </div>
          </div>
        </div>
      );
}

export default Upload;


import React from 'react';
import Upload from './components/Upload';
import Display from './components/Display';
import Callback from './components/Callback';
import Nav from './components/Nav';
//import {Router, Route, browserHistory} from 'react-router';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { requireAuth } from './utils/AuthService';


const history = createHistory();

function App() {
  return (
    <div className="container">
      <Router history={history} >
          <Nav />
          <Switch> 
            <Route exact path="/" component={Display}/>
            <Route exact path="/upload" component={Upload} onEnter={requireAuth} />
            <Route exact path="/callback" component={Callback} />
        </Switch>
      </Router>
        
    </div>
  );
}

export default App;
